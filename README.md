# XCMS and CAMERA tutorial

The XCMS presentation was built for the [*EMN Workshop: MS Metabolomics Data Processing*](http://metabolomics2017.org/program/workshops) given at the 2017 conference of the Metabolomics Society. A video of the workshop (5) can be found on the [Metabolomics Society website](http://metabolomicssociety.org/site-map/articles/88-videos/262-2017-conference-workshop-videos-public).<br>
I have later added the CAMERA tutorial that uses the peaklist created in the XCMS tutorial.

You can watch the slides of the presentations at:

* [XCMS](https://stanstrup-teaching.gitlab.io/XCMS-course/1-XCMS.html)
* [CAMERA](https://stanstrup-teaching.gitlab.io/XCMS-course/2-CAMERA.html)

We would like to thank Drs. Richard and Nichole Reisdorph and their [*Mass Spectrometry Laboratory*](http://www.ucdenver.edu/academics/colleges/pharmacy/Research/CoreFacilities/Spectrometry/Pages/default.aspx) for providing us with the data files for use in this presentation.  This data was acquired as part of their [*Metabolomics Workshops*](http://www.ucdenver.edu/academics/colleges/pharmacy/AboutUs/NewsEvents/MetabolomicsWorkshop/Pages/default.aspx), which are held annually in Denver, Colorado, U.S.A.
